import PySimpleGUI as sg
from icons import *
import time

feedback_file_name = 'feedback.csv'

layout = [[sg.Image(data=MSM,
                    background_color='white')],
          #[sg.Text("Swipe your card to record attendance:",
          #         text_color='black',
          #         background_color='white')],
          [sg.Text("Please give your feedback:",
                   font=('bold', 20),
                   text_color='black',
                   background_color='white',
                   pad=10)],
          [sg.Button('',
                     image_data=positive,
                     button_color=("white","white"),
                     border_width=0,
                     key='-POSITIVE-',
                     pad=20),
           sg.Button('',
                     image_data=neutral,
                     button_color=("white","white"),
                     border_width=0,
                     key='-NEUTRAL-',
                     pad=20),
           sg.Button('',
                     image_data=negative,
                     button_color=("white","white"),
                     border_width=0,
                     key='-NEGATIVE-',
                     pad=20)],
          [sg.Text('test', size=(40,1),
                   key='-TIME-',
                   text_color='blue',
                   background_color='white',
                   pad=10)],
          [sg.Button(bind_return_key=True,
                     visible=False)]
          ]



# Create the window
window = sg.Window('Materials Science Swipe In',
                   layout,
                   margins=(10,10),
                   #disable_minimize=True,
                   #disable_close=False,
                   text_justification='center',
                   #keep_on_top=True,
                   #modal=True,
                   element_justification='center',
                   finalize=True,
                   background_color='white',
                   size=(800,480))
#window.maximize()
n = 0
colours = ['red','darkorange','yellow4','green','blue','indigo']
# Display and interact with the Window using an Event Loop
while True:
    
    event, values = window.read()
    
    # See if user wants to quit or window was closed
    if event == sg.WINDOW_CLOSED or event == 'Quit':# or values['-INPUT-'] == 'quit':
        break
    # Output a message to the window
    n += 1
    window['-TIME-'].update('   ')
    window.refresh()
    time.sleep(0.1)
    window['-TIME-'].update('feedback recorded: ' + time.ctime(),
                            text_color=colours[n % 6])
    #start = time.time()
    
    with open(feedback_file_name,'at') as f:
        f.write(event + ',' + time.ctime() + '\n')
# Finish up by removing from the screen
window.close()
